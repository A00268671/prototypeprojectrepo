package com.ait.maths;

public class MathsMethods {
	
	public int add(int a, int b) {
		return a + b;
	}
	
	public int subtract(int a, int b) {
		return a - b;
	}
	
	public int multiply(int a, int b) {
		return a * b;
	}
	
	public int divide(int a, int b) {
		return a / b;
	}
	
	public int percentage(int a, int b) {
		return (int)(((double)a /(double) b) * 100);
	}
	
	public int modulus(int a, int b) {
		return a % b;
	}
	
	public int randomMultiply(int a, int b) {
		return a * b;
	}

}

