package com.ait.maths;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MathsMethodsTest {

	private MathsMethods mathsMethods;
	
	private NumberGenerator numberServer = mock(NumberGenerator.class);

	@Before
	public void setUp() throws Exception {
		mathsMethods = new MathsMethods();
	}

	@Test
	public void testAdd() {
		System.out.println("Testing testAdd() method...");
		assertEquals(7, mathsMethods.add(3, 4));
	}
	
	@Test
	public void testSubtract() {
		System.out.println("Testing testSubtract() method...");
		assertEquals(3, mathsMethods.subtract(5, 2));
	}
	
	@Test
	public void testMultiply() {
		System.out.println("Testing testMultiply() method...");
		assertEquals(12, mathsMethods.multiply(2, 6));
	}
	
	@Test
	public void testDivide() {
		System.out.println("Testing testDivide() method...");
		assertEquals(4, mathsMethods.divide(12, 3));
	}
	
	@Test
	public void testPercentage() {
		assertEquals(25, mathsMethods.percentage(2, 8));
	}
	
	@Test
	public void testModulus() {
		assertEquals(1, mathsMethods.divide(3, 2));
	}
	
	@Test
	public void testRandomMultiply() {
		when(numberServer.randomNumberGenerator()).thenReturn(36);
		assertEquals(108, mathsMethods.randomMultiply(3, numberServer.randomNumberGenerator()));
	}

}
